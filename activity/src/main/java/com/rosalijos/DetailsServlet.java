package com.rosalijos;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class DetailsServlet extends HttpServlet {

	private static final long serialVersionUID = -1544790225938134386L;

	public void init() throws ServletException {
		System.out.println("*************************************");
		System.out.println("Details Servlet has been initialized.");
		System.out.println("*************************************");
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		
		// via servlet context parameter
		ServletContext servletContext = getServletContext();
		String branding = servletContext.getInitParameter("branding");
		
		// via system properties
		String firstname = System.getProperty("firstname");
		
		// via http session
		HttpSession session = request.getSession();
		String lastname = session.getAttribute("lastname").toString();
		
		// via servlet context
		String email = servletContext.getAttribute("email").toString();
		
		// via send redirect
		String contact = request.getParameter("contact");
		
		out.println(
				"<h1>" + branding + "</h1>" + 
				"<p>First Name: " + firstname + "</p>" +
				"<p>Last Name: " + lastname + "</p>" +
				"<p>Email: " + email + "</p>" +
				"<p>Contact: " + contact + "</p>" 
			);
		
	}
	
	public void destroy() {
		System.out.println("***********************************");
		System.out.println("Details Servlet has been destroyed.");
		System.out.println("***********************************");
	}
}
