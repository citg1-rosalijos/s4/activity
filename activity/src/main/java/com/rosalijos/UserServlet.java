package com.rosalijos;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	private static final long serialVersionUID = 3641394635116934105L;

	public void init() throws ServletException {
		System.out.println("**********************************");
		System.out.println("User Servlet has been initialized.");
		System.out.println("**********************************");
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		// via system properties
		System.getProperties().put("firstname", request.getParameter("firstname"));
		
		// via http session
		HttpSession session = request.getSession();
		session.setAttribute("lastname", request.getParameter("lastname"));
		
		// via servlet context set attribute
		ServletContext servletContext = getServletContext();
		servletContext.setAttribute("email", request.getParameter("email"));
		
		// via send redirect
		response.sendRedirect("details?contact=" + request.getParameter("contact"));
		
	}
	
	public void destroy() {
		System.out.println("********************************");
		System.out.println("User Servlet has been destroyed.");
		System.out.println("********************************");
	}
}
